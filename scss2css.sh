#!/bin/sh
set -o errexit

if [ $# != 2 ]; then
	printf 'Usage: %s source.scss destination.css\n' "$(basename "$0")"
	exit 1
fi

source="$1"
destination="$2"

if [ ! -e "$source" ]; then
	printf '\033[1;31mErreur !\033[0m\n'
	printf '%s est introuvable.\n' "$source"
	exit 1
fi

check_dependency() {
	local command
	local package
	command="$1"
	package="$2"
	if ! command -v $command 1>/dev/null 2>&1; then
		printf '\033[1;31mErreur !\033[0m\n'
		printf 'La commande "%s" est introuvable.\n' "$command"
		printf 'Installez-la en lançant la commande suivante :\n'
		printf 'sudo apt install %s\n' "$package"
		exit 1
	fi
}

check_dependency 'sassc' 'sassc'
check_dependency 'minify' 'minify'

temp_dir="$(mktemp --directory)"
temp_file="$temp_dir/style.css"

printf 'Compilation de %s vers %s…\n' "$source" "$destination"
sassc "$source" "$temp_file"
mkdir --parents "$(dirname "$destination")"
minify -o "$destination" "$temp_file"
rm "$temp_file"
rmdir "$temp_dir"
printf '\033[1;32mOK\033[0m\n'

exit 0
